const getSum = (str1, str2) => {
  if ((typeof str1 === 'object') || (typeof str2 === 'object'))
    return false
  
  if (Array.isArray(str1) || Array.isArray(str2))
    return false;
  
  if (!isValidString(str1) || !isValidString(str2))
    return false;

  if (str1.length == 0)
    return str2;

  if (str2.length == 0)
    return str1;
  
  let res = '';

  for (let i = 0; i < str1.length; i++) {
    res += parseInt(str1[i]) + parseInt(str2[i]);
  }

  function isValidString(str) {
    for (let char of str) {
      if (isNaN(parseInt(char)))
        return false;
    }

    return true
  }
  
  return res;
}

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let posts = 0;
  let comments = 0;

  for (let post of listOfPosts) {
    if (post.author === authorName)
      posts++;
    
    if (typeof post.comments !== 'undefined')
      for (let comment of post.comments) {
        if (comment.author == authorName)
          comments++;
      }
  }

  return `Post:${posts},comments:${comments}`;
}

const tickets = (people)=> {
  let money = 0;
  const cost = 25;

  for (let person of people) {
    let change = person - cost;
    
    if (money >= change) {
      money += cost;
    } else {
      return 'NO';
    }
  }

  return 'YES';
};


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
